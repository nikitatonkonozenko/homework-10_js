const tabsTitles = document.querySelectorAll(".tabs-title")
const tabsContent = document.querySelectorAll(".tabs-content-item")

tabsTitles.forEach(elem =>{
    elem.addEventListener("click", (event) => {
        const target = event.target.dataset.target
        const targetTabContent = document.querySelector(`#${target}`)
        tabsTitles.forEach(elem => {
            elem.classList.remove("active")
        })
        tabsContent.forEach(elem => {
            elem.classList.remove("active")
        })
        event.target.classList.add("active")
        targetTabContent.classList.add("active")
    })
})